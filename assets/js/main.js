//load DOM 
window.addEventListener('load', init);

//initialize functions when DOM is loaded
function init() {

    getTopArtists();

};


function getTopArtists () {

    var urlTopArtist = 'http://ws.audioscrobbler.com/2.0/?method=chart.gettopartists&limit=10&api_key=de6184b5a1362cc69512027982282428&format=json';
    
       
    $.getJSON(urlTopArtist)
        .done( function(data){
            console.log(data.artists.artist);
            displayArtists(data.artists.artist);
        })

        .fail( function(){
            console.log ('fail');
        });

    
};





function displayArtists (artist) {

        for (var i = 0; i < artist.length; i++) {
            var element = artist[i];

            var info = '"info"';
            var target = '"_blank"';

            var div = document.createElement('div');
            div.setAttribute('class', 'artist');
            div.innerHTML = '<img src='+artist[i].image[3]['#text']+'/>' +
                            '<div class='+info+'><h2>'+artist[i].name+'</h2>' +
                            '<p>Listeners: '+artist[i].listeners+'<br />' +
                                'Playcount: '+artist[i].playcount+'</p>'+
                            "<a href="+artist[i].url+" target="+target+">Artist's Page</a></div>";

            //appendChild elementen
            var ph = document.getElementById('ph');
            ph.appendChild(div);
        }    
};